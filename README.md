Dependencies:
- virtualbox- must be installed
- ansible must be installed
- vagrant must be installed 

Set the Environment:

To do this you have to modify MASTER and NODES variables in the vagrant file to specify how many masters and nodes you want to be deployed. After that you have to modify the ansible inventory (./ansible/hosts.ini) to have the new nodes and masters that will be 

Create the clusters:

Just run: vagrant up

Check that the clusters are running properly:

vagrant ssh master1
sudo -i
export KUBECONFIG=/etc/kubernetes/admin.conf &&  kubectl get nodes

vagrant ssh master2
sudo -i
export KUBECONFIG=/etc/kubernetes/admin.conf &&  kubectl get nodes